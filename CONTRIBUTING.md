# Contributors

Please feel free to submit an issue or pull request. To develop, you'll need to test the script locally with your own 
AWS Credentials. 