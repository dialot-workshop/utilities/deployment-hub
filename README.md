# Deployment-Hub

## Description
This project is used by the main <Stack-Name> application. 
The goal of this project is to be used has a centralized hub for all deployment by <Stack-Name>. It uses the feature 
provider by Gitlab runners to minimize the operational overhead of managing temporary tasks.

This service also support deploying to multiple Cloud Provider by using the <Stack-Name> Credentials Provider Service .

The main idea when created this service was to offer an easy and reliable way to deploy Terraform plans to any Cloud 
Provider with it.

---

**⚠️ This is not production-ready software. This project is in active development ⚠️**

---

## Visuals
todo

## Support
Help is always appreciated. Every Merge Requests or open issues will be review by the maintainers and updated 
accordingly.

## Roadmap
This project is only in starting phase, multiple features are yet to be implemented.
- Make desired Architecture Diagram
- Create Terraform var file dynamically based on the input
- Get Terraform Plan path based on the input
- Integration with the Credentials Provider Service
- Use custom docker image
- Support Azure deployment
- Support Google Cloud deployment

## Contributing
Please feel free to submit an issue or pull request. To develop, you'll need to test the script locally with your own 
AWS Credentials. 

## Authors and acknowledgment
[Nicolas Bazinet](@nbazinet)

## License
[GNU GPLv3](./LICENSE)

## Project status
In development
